# pyteam

After having a running app engine app.

Using YAML file as configuration source (example as blow ):
Use your ldap as a key
start time and end time (9 am to 5 pm)
You can use ldap+bla@google.com to make multiple users

Use a task queue in App engine to send an email to the user every hour.

Please include any test possible for yaml file and send email.


Client has a roaster list with engineers names, schedules e.g: (Sydney time)

---
| Tom   | 8am - 3pm  |
| Jerry | 8am -3pm   |
| Nancy | 11am - 6pm |
| Peter | 11am - 6pm |
| Brian | 3pm - 10pm |
| Lucy  | 3pm - 10pm |
---

To support the client’s global business, the engineers takes customer’s tickets to work on. The tickets are coming into a queue. Please design a program, assigns the tickets to engineers according to their shift.

For example, a user from overseas may submit a ticket, on 7pm Sydney time, the program will assign the ticket to either Brian or Lucy, so the engineers on shift can take action on the ticket.


# How to deploy main app (email sender)
```
$ gcloud app deploy
$ gcloud app deploy --project=EPAM
```

# How to deploy cron jobs
```
$ gcloud app deploy cron.yaml
```

# Run dev server (local server)
```
$ dev_appserver.py app.yaml
```

Visit the Cloud Platform Console Task Queues page to view your queues and cron jobs.
https://console.cloud.google.com/appengine/taskqueues/cron?project=takuya-jesus-1234
