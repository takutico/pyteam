import webapp2

from datetime import datetime
from roaster import Roaster
from send_email import MySendEmail


class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, PyTeam 2.7!')


class SendEmail(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        myemail = MySendEmail()
        myroaster = Roaster()
        roaster = myroaster.getRoasterFromFile()
        # to_list = []

        current_hour = datetime.now().hour
        for i in roaster:
            # print('current hour: {}, roaster: {}, tse: {}'.format(current_hour, i[1], i[0]))
            if myroaster.isBetweenTimes(current_hour, i[1]):
                print('{} is working at {}'.format(i[0], current_hour))
                if '+' in i[0]:
                    for j in i[0].split('+'):
                        myemail.myEmailSender(j)
                else:
                    myemail.myEmailSender(i[0])
                # TODO: add all emails into a list and send it
                # to_list.append(i[0])
        self.response.write(roaster)


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/send-email', SendEmail),
], debug=True)


# To run locally
def main():
    from paste import httpserver
    httpserver.serve(app, host='127.0.0.1', port='8080')

if __name__ == '__main__':
    main()
