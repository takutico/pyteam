import webtest

import main


def test_get():
    app = webtest.TestApp(main.app)

    response = app.get('/')

    assert response.status_int == 200
    assert response.body == 'Hello, PyTeam 2.7!'




'''
Local Unit test: https://cloud.google.com/appengine/docs/standard/python/tools/localunittesting
Testing Handlers: https://cloud.google.com/appengine/docs/standard/python/tools/handlertesting
'''
