class Roaster:
    def __init__(self, file_name='roaster.yaml'):
        self.file_name = file_name

    def getRoasterFromFile(self, file_name='roaster.yaml'):
        """
        Read roaster file and return a list of users
        """
        test = []
        with open(file_name) as roaster:
            for i in roaster:
                if i[0] != '#':  # skip comments
                    # Normalize yaml file
                    tmp = ''.join(i.split())  # remove all spaces
                    tmp = tmp.strip().split(':', 1)  # split key and value
                    test.append(tmp)
                    # we can use yield
                    # yield i.split('\n')[0]
        return test


    def isBetweenTimes(self, current_hour, times_from_to):
        '''
        return true if current_hour is between times_from_to
        '''
        start, end = map(int, times_from_to.split('-'))
        if start < end:
            return start <= current_hour <= end
        elif start > end:
            return start <= current_hour <= 24 or \
                   0 <= current_hour <= end
        else:
            return False
