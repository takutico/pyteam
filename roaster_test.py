from roaster import Roaster
import unittest

class TestRoasterMethods(unittest.TestCase):

    def test_getRoasterFromFile(self):
        test_file = 'roaster_fake.yaml'
        roaster = Roaster().getRoasterFromFile('roaster_fake.yaml')
        self.assertTrue(len(roaster) == 4)
        self.assertTrue(['takutico', '20-7'] in roaster)

    def test_isBetweenTimes(self):
        ro = Roaster()
        self.assertTrue(ro.isBetweenTimes(3, '1-8'))
        self.assertFalse(ro.isBetweenTimes(0, '1-8'))
        self.assertFalse(ro.isBetweenTimes(9, '1-8'))
        self.assertTrue(ro.isBetweenTimes(10, '8-16'))
        self.assertFalse(ro.isBetweenTimes(4, '8-16'))
        self.assertFalse(ro.isBetweenTimes(20, '8-16'))
        self.assertTrue(ro.isBetweenTimes(20, '17-3'))
        self.assertFalse(ro.isBetweenTimes(4, '17-3'))
        self.assertFalse(ro.isBetweenTimes(11, '17-3'))

    def test_roaster(self):
        test_file = 'roaster_fake.yaml'
        ro = Roaster()
        roaster = ro.getRoasterFromFile('roaster_fake.yaml')
        for i in roaster:
            if i[0] == 'yamaguchi.padilla':
                self.assertTrue(ro.isBetweenTimes(11, i[1]))
            if i[0] == 'tyampad':
                self.assertTrue(ro.isBetweenTimes(15, i[1]))
            if i[0] == 'iamfake':
                self.assertTrue(ro.isBetweenTimes(19, i[1]))
            if i[0] == 'takutico':
                self.assertTrue(ro.isBetweenTimes(1, i[1]))


if __name__ == '__main__':
    unittest.main()
