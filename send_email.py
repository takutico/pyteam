from google.appengine.api import app_identity
from google.appengine.api import mail


class MySendEmail:
    def __init__(self):
        self.to = 'test@test.com'

    def myEmailSender(self, to):
        """
        TODO: move to a different class
        """
        # [START send_message]
        # sender_address = ('Example.com Support <{}@appspot.gserviceaccount.com>'.format(
        #                 app_identity.get_application_id()))
        sender_address = 'support@example.com'
        message = mail.send_mail()
        message.sender = sender_address
        message.to = "{} <{}@gmail.com>".format(to, to)
        message.subject = "Your have a new case"
        message.body = "Your got a new case"
        message.send()

        print('email sent to {}@gmail.com'.format(to))
        # [END send_message]
