from send_email import MySendEmail
import unittest

class TestSendMail(unittest.TestCase):

    def test_send_mail(self):
        my_email = MySendEmail()
        response = my_email.myEmailSender('test@test.com')
        self.assertEqual(response, '200')

if __name__ == '__main__':
    unittest.main()

'''
import webtest

import send_mail


def test_send_mail(testbed):
    testbed.init_mail_stub()
    testbed.init_app_identity_stub()
    app = webtest.TestApp(send_mail.app)
    response = app.get('/send_mail')
    assert response.status_int == 200
    assert 'Sent an email to Albert.' in response.body
'''
