### Todo

- [ ] Unit tests
- [ ] Fix email sender issue
- [ ] Email retry
- [x] Separate functionalities in different classes
- [x] Read mulitple ldap's in one line "ldap+ldap@email.com"
